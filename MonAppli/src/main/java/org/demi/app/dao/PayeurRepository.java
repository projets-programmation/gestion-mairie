package org.demi.app.dao;

import org.demi.app.entities.Payeur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PayeurRepository extends JpaRepository<Payeur, Long> {

}
