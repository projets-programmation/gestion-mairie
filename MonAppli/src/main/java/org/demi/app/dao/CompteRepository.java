package org.demi.app.dao;

import org.demi.app.entities.Compte_payeur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRepository extends JpaRepository<Compte_payeur, Long> {

}
