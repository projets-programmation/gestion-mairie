package org.demi.app.dao;

import org.demi.app.entities.Activite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ActiviteRepository extends JpaRepository<Activite, Long> {

}
