package org.demi.app.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Payeur implements Serializable {
	
	@Id @GeneratedValue
	private Long id;
	private String nom;
	private String prenom;
	private int numero;
	private String adresse;
	@ManyToOne
	@JoinColumn(name = "ID_AGENT")
	private Agent agent;
	@OneToMany(mappedBy = "payeur",fetch = FetchType.LAZY)
	private Collection<Activite> activites;

	public Payeur() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Payeur(String nom, String prenom, int numero, String adresse, Agent agent) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numero = numero;
		this.adresse = adresse;
		this.agent = agent;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Collection<Activite> getActivites() {
		return activites;
	}

	public void setActivites(Collection<Activite> activites) {
		this.activites = activites;
	}
	
	

}
