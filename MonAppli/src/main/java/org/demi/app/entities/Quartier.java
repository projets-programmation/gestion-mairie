package org.demi.app.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("Q")
public class Quartier extends Activite {
	private String nomType;
	private double tranche_1;
	private Date date_1;
	private double tranche_2;
	private Date date_2;
	private double tranche_3;
	private Date date_3;
	private double tranche_4;
	private Date date_4;
	public Quartier(String nom, String localisation, Payeur payeur, String nomType, double tranche_1, Date date_1,
			double tranche_2, Date date_2, double tranche_3, Date date_3, double tranche_4, Date date_4) {
		super(nom, localisation, payeur);
		this.nomType = nomType;
		this.tranche_1 = tranche_1;
		this.date_1 = date_1;
		this.tranche_2 = tranche_2;
		this.date_2 = date_2;
		this.tranche_3 = tranche_3;
		this.date_3 = date_3;
		this.tranche_4 = tranche_4;
		this.date_4 = date_4;
	}
	public String getNomType() {
		return nomType;
	}
	public void setNomType(String nomType) {
		this.nomType = nomType;
	}
	public double getTranche_1() {
		return tranche_1;
	}
	public void setTranche_1(double tranche_1) {
		this.tranche_1 = tranche_1;
	}
	public Date getDate_1() {
		return date_1;
	}
	public void setDate_1(Date date_1) {
		this.date_1 = date_1;
	}
	public double getTranche_2() {
		return tranche_2;
	}
	public void setTranche_2(double tranche_2) {
		this.tranche_2 = tranche_2;
	}
	public Date getDate_2() {
		return date_2;
	}
	public void setDate_2(Date date_2) {
		this.date_2 = date_2;
	}
	public double getTranche_3() {
		return tranche_3;
	}
	public void setTranche_3(double tranche_3) {
		this.tranche_3 = tranche_3;
	}
	public Date getDate_3() {
		return date_3;
	}
	public void setDate_3(Date date_3) {
		this.date_3 = date_3;
	}
	public double getTranche_4() {
		return tranche_4;
	}
	public void setTranche_4(double tranche_4) {
		this.tranche_4 = tranche_4;
	}
	public Date getDate_4() {
		return date_4;
	}
	public void setDate_4(Date date_4) {
		this.date_4 = date_4;
	}

	

}
