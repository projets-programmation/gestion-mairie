package org.demi.app.entities;

import java.util.Date;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("A")
public class Annuel extends Activite {
	private String nomType;
	private double tranche_1;
	private Date date_1;
	
	public Annuel(String nom, String localisation, Payeur payeur, String nomType, double tranche_1, Date date_1) {
		super(nom, localisation, payeur);
		this.nomType = nomType;
		this.tranche_1 = tranche_1;
		this.date_1 = date_1;
	}

	public String getNomType() {
		return nomType;
	}

	public void setNomType(String nomType) {
		this.nomType = nomType;
	}

	public double getTranche_1() {
		return tranche_1;
	}

	public void setTranche_1(double tranche_1) {
		this.tranche_1 = tranche_1;
	}

	public Date getDate_1() {
		return date_1;
	}

	public void setDate_1(Date date_1) {
		this.date_1 = date_1;
	}
	
}
