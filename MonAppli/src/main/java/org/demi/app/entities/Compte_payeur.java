package org.demi.app.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Compte_payeur implements Serializable {
	@Id @GeneratedValue
	private Long id_compte;
	private Date dateCreation;
	private double aPayer;
	private double reste;
	@OneToOne
	@JoinColumn(name = "NUM_ACTIVITE")
	private Activite activite;
	@OneToMany(mappedBy = "compte_payeur",fetch = FetchType.LAZY)
	private Collection<Operation> operations;
	public Compte_payeur() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Compte_payeur(Date dateCreation, double aPayer, double reste, Activite activite) {
		super();
		this.dateCreation = dateCreation;
		this.aPayer = aPayer;
		this.reste = reste;
		this.activite = activite;
	}

	public Long getId_compte() {
		return id_compte;
	}
	public void setId_compte(Long id_compte) {
		this.id_compte = id_compte;
	}
	public Date getDateCreation() {
		return dateCreation;
	}
	public void setDateCreation(Date dateCreation) {
		this.dateCreation = dateCreation;
	}
	public double getaPayer() {
		return aPayer;
	}
	public void setaPayer(double aPayer) {
		this.aPayer = aPayer;
	}
	public double getReste() {
		return reste;
	}
	public void setReste(double reste) {
		this.reste = reste;
	}
	public Activite getActivite() {
		return activite;
	}
	public void setActivite(Activite activite) {
		this.activite = activite;
	}
	public Collection<Operation> getOperations() {
		return operations;
	}
	public void setOperations(Collection<Operation> operations) {
		this.operations = operations;
	}
	
	

}
