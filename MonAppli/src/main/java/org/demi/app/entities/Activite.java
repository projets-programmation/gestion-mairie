package org.demi.app.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE_ACT",discriminatorType = DiscriminatorType.STRING,length = 1)
public abstract class Activite implements Serializable {
	@Id @GeneratedValue
	private Long num_activite;
	private String nom;
	private String localisation;
	@ManyToOne
	@JoinColumn(name = "ID_PAYEUR")
	private Payeur payeur;
	@OneToMany(mappedBy = "activite",fetch = FetchType.LAZY)
	private Collection<Compte_payeur> compte_payeur;
	
	public Activite() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Activite(String nom, String localisation, Payeur payeur) {
		super();
		this.nom = nom;
		this.localisation = localisation;
		this.payeur = payeur;
	}



	public Long getNum_activite() {
		return num_activite;
	}

	public void setNum_activite(Long num_activite) {
		this.num_activite = num_activite;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getLocalisation() {
		return localisation;
	}

	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	public Payeur getPayeur() {
		return payeur;
	}

	public void setPayeur(Payeur payeur) {
		this.payeur = payeur;
	}

	public Collection<Compte_payeur> getCompte_payeur() {
		return compte_payeur;
	}

	public void setCompte_payeur(Collection<Compte_payeur> compte_payeur) {
		this.compte_payeur = compte_payeur;
	}

}