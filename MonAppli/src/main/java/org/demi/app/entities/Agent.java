package org.demi.app.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Agent implements Serializable {
	@Id @GeneratedValue
	private Long id_agent;
	private String nom;
	private String prenom;
	private int numero;
	@OneToMany(mappedBy = "agent",fetch = FetchType.LAZY)
	private Collection<Payeur> payeurs;
	public Agent() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Agent(String nom, String prenom, int numero) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.numero = numero;
	}

	public Long getId_agent() {
		return id_agent;
	}
	public void setId_agent(Long id_agent) {
		this.id_agent = id_agent;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Collection<Payeur> getPayeurs() {
		return payeurs;
	}
	public void setPayeurs(Collection<Payeur> payeurs) {
		this.payeurs = payeurs;
	}
	

}
