package org.demi.app;

import java.util.Date;

import org.demi.app.dao.ActiviteRepository;
import org.demi.app.dao.AgentRepository;
import org.demi.app.dao.CompteRepository;
import org.demi.app.dao.PayeurRepository;
import org.demi.app.entities.Activite;
import org.demi.app.entities.Agent;
import org.demi.app.entities.Compte_payeur;
import org.demi.app.entities.Mensuel;
import org.demi.app.entities.Payeur;
import org.demi.app.entities.Quartier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonAppliApplication implements CommandLineRunner {
	
	@Autowired
	private PayeurRepository payeurRepository;
	@Autowired
	private AgentRepository agentRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private ActiviteRepository activiteRepository;
	
	public static void main(String[] args) {
		SpringApplication.run(MonAppliApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		
		  Agent a1 = agentRepository.save(new Agent("jo", "ivan", 698385951)); Agent a2
		  = agentRepository.save(new Agent("man", "yannick", 678549664));
		  
		  Payeur p1 = payeurRepository.save(new Payeur("hassan", "fred", 687767845,
		  "NGUEME", a1)); Payeur p2 = payeurRepository.save(new Payeur("merci", "marc",
		  687723445, "SOKOLO", a1)); Payeur p3 = payeurRepository.save(new
		  Payeur("hassan", "fred", 687767845, "NGUEME", a2));
		  
		  Activite act1 = activiteRepository.save(new Quartier("easymarket", "sokolo",
		  p2, "BAR", 25000, new Date(), 12000, new Date(), 80000, new Date(), 76000,
		  new Date())); compteRepository.save(new Compte_payeur(new Date(), 193000,
		  12000, act1));
		 
		
	}

}
